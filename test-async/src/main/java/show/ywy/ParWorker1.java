package show.ywy;

import cn.hutool.core.lang.Console;
import com.jd.platform.async.callback.ICallback;
import com.jd.platform.async.callback.IWorker;
import com.jd.platform.async.worker.ResultState;
import com.jd.platform.async.worker.WorkResult;
import com.jd.platform.async.wrapper.WorkerWrapper;

import java.util.Map;

public class ParWorker1 implements IWorker<String, Long>, ICallback<String, Boolean> {

    @Override
    public void begin() {
        // 开始执行ParWork1
        Console.log("开始执行ParWork1");
    }

    @Override
    public void result(boolean success, String param, WorkResult<Boolean> workResult) {
        Boolean result = workResult.getResult();
        System.out.println("result = " + result);
        Exception ex = workResult.getEx();
        System.out.println("ex = " + ex.getMessage());
        ResultState resultState = workResult.getResultState();
        System.out.println("resultState = " + resultState);
        if (success) {
            System.out.println("Success! ->" + param);
        }else {
            System.out.println("Not Success! ->" + param);
        }
    }

    @Override
    public Long action(String object, Map<String, WorkerWrapper> allWrappers) {
        System.out.println("你的入参 : " + object);
        return 1L;
    }

    @Override
    public Long defaultValue() {
        return 0L;
    }
}